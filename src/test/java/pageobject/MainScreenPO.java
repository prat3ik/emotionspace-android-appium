package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utils.AppiumUtils;

public class MainScreenPO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    public MainScreenPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/button_login")
    AndroidElement logInButton;

    public LoginPO tapOnLogInButton() {
        logInButton.click();
        return new LoginPO(driver);
    }

    public boolean isMainScreenDisplayed() {
        return AppiumUtils.isElementDisplayed(logInButton);
    }
}
