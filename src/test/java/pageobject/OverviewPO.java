package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 07/01/19
 * @project POM_Automation_Framework
 */
public class OverviewPO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    protected OverviewPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(accessibility = "Open Drawer")
    AndroidElement hamburgerIcon;

    public void tapOnHamburgerIcon() {
        hamburgerIcon.click();
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='My profile']")
    AndroidElement myProfileOption;

    public MyProfilePO tapOnMyProfileOption() {
        myProfileOption.click();
        return new MyProfilePO(driver);
    }


}
