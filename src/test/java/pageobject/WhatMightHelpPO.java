package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utils.AppiumUtils;

import java.util.List;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 07/01/19
 * @project POM_Automation_Framework
 */
public class WhatMightHelpPO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    protected WhatMightHelpPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/button_see_all_needs")
    AndroidElement seeAllOptionsButton;

    public void tapOnSeeAllOptionsButton(){
        seeAllOptionsButton.click();
    }

    @AndroidFindBy(xpath= "//android.widget.TextView[@text='My treatment']")
    AndroidElement myTreatmentOption;

    public void tapOnMyTreatmentOption(){
        myTreatmentOption.click();
    }


    @AndroidFindBy(className= "android.widget.RadioButton")
    List<AndroidElement> filteredOptions;

    public void tapOnParticularFilteredOptions(int index){
        filteredOptions.get(index).click();
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_done_widget")
    AndroidElement saveButton;

    public EmotionPO tapOnSaveButton() {
        saveButton.click();
        return new EmotionPO(driver);
    }


    public void scrollDownToScreen() {
        AppiumUtils.verticalScroll(driver);
    }
}
