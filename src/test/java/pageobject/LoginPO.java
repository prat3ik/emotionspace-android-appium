package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;


/**
 * Year: 2018-19
 *
 */
public class LoginPO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    public LoginPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/edit_text_enter_email")
    AndroidElement usernameTextField;

    public LoginPO setUsernameTextField(String username) {
        usernameTextField.sendKeys(username);
        return this;
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/edit_text_login_pwd")
    AndroidElement passwordTextField;

    public LoginPO setPasswordTextField(String password) {
        passwordTextField.sendKeys(password);
        return this;
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/button_login_next")
    AndroidElement loginButton;

    public DashboardPO tapOnLoginButton() {
        loginButton.click();
        return new DashboardPO(driver);
    }

    public boolean isLoginPageDisplayed() {
        return loginButton.isDisplayed();
    }

}

