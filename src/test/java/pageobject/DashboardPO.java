package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utils.AppiumUtils;


/**
 * Year: 2018-19
 */
public class DashboardPO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    public DashboardPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Happy']")
    AndroidElement happyTextView;

    /**
     * This method will click on Logout textview.
     */
    public void tapOnHappyTextView() {
        happyTextView.click();
        waitUtils.waitForElementToBeVisible(nextButton, driver);
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/button_select_intensity")
    AndroidElement nextButton;

    /**
     * This method will click on Next button.
     */
    public EmotionPO tapOnNextButton() {
        nextButton.click();
        return new EmotionPO(driver);
    }

    public void scrollUpToVeryHappy() {
        AppiumUtils.verticalScroll(driver);
    }

}
