package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/01/19
 * @project POM_Automation_Framework
 */
public class WhatsHappenedPO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    protected WhatsHappenedPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/text_view_reason_parent_item_name")
    List<AndroidElement> itsMyTreatmentTextView;

    public void tapOnItsMyTreatmentTextView() {
        itsMyTreatmentTextView.get(0).click();
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_see_more_option")
    AndroidElement seeMoreOptionButton;

    public void tapOnseeMoreOptionButton() {
        seeMoreOptionButton.click();
    }

    @AndroidFindBy(className = "android.widget.CheckBox")
    List<AndroidElement> checkboxes;

    public AndroidElement getIHaveHadSomeGoodNewsCheckBox() {
        return checkboxes.get(0);
    }

    public void checkIHaveHadSomeGoodNewsCheckBox() {
        getIHaveHadSomeGoodNewsCheckBox().click();
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_done_widget")
    AndroidElement saveButton;

    public EmotionPO tapOnSaveButton() {
        saveButton.click();
        return new EmotionPO(driver);
    }


}
