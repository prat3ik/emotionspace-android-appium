package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 07/01/19
 * @project POM_Automation_Framework
 */
public class MyProfilePO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    protected MyProfilePO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/rl_my_account")
    AndroidElement myAccountOption;

    public void tapOnMyAccountOption(){
        myAccountOption.click();
    }

    @AndroidFindBy(id = "com.pfizer.ie.EmotionSpace:id/btn_view_sign_out")
    AndroidElement logOutButton;

    protected void tapOnLogOutButton(){
        logOutButton.click();
    }

    @AndroidFindBy(id = "android:id/button1")
    AndroidElement logOutButtonOnDialog;

    protected void tapOnLogOutButtonOnDialog(){
        logOutButtonOnDialog.click();
    }

    public MainScreenPO doLogOut(){
        tapOnLogOutButton();
        tapOnLogOutButtonOnDialog();
        return new MainScreenPO(driver);
    }
}
