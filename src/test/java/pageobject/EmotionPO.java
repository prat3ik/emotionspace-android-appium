package pageobject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utils.AppiumUtils;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/01/19
 * @project POM_Automation_Framework
 */
public class EmotionPO extends BasePO {
    /**
     * A base constructor that sets the page's androidDriver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium androidDriver created in the beforesuite method.
     */
    protected EmotionPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "done")
    AndroidElement doneButton;

    public boolean isDoneButtonDisplayed(){
        return AppiumUtils.isElementDisplayed(doneButton);
    }

    @AndroidFindBy(id ="com.pfizer.ie.EmotionSpace:id/text_view_add_cause")
    AndroidElement whyDoYouFeelLikeThatOption;

    public WhatsHappenedPO tapOnWhyDoYouFeelLikeThatOption(){
        whyDoYouFeelLikeThatOption.click();
        return new WhatsHappenedPO(driver);
    }

    @AndroidFindBy(id="com.pfizer.ie.EmotionSpace:id/image_need_check_mark")
    AndroidElement whatMightHelpPlusIcon;

    public WhatMightHelpPO tapOnWhatMightHelpPlusIcon(){
        whatMightHelpPlusIcon.click();
        return new WhatMightHelpPO(driver);
    }

    @AndroidFindBy(id="com.pfizer.ie.EmotionSpace:id/btn_done_widget")
    AndroidElement iAmFinishedButton;

    public OverviewPO tapOnIAmFinishedButton(){
        iAmFinishedButton.click();
        return new OverviewPO(driver);
    }
}
