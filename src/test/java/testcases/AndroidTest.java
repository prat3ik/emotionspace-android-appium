package testcases;

import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobject.*;


import java.net.MalformedURLException;
import java.net.URL;

/**
 * Year: 2018-19
 */
public class AndroidTest extends BaseTest {

    @Test
    public void testEmotion() {
        String username = "estest@emotion.com";
        String password = "abcd1234";

        MainScreenPO mainScreenPO = new MainScreenPO(androidDriver);
        LoginPO loginPO = mainScreenPO.tapOnLogInButton();
        DashboardPO dashboardPO = loginPO.setUsernameTextField(username).setPasswordTextField(password).tapOnLoginButton();
        dashboardPO.tapOnHappyTextView();
        dashboardPO.scrollUpToVeryHappy();

        EmotionPO emotionPO = dashboardPO.tapOnNextButton();

        WhatsHappenedPO whatsHappenedPO = emotionPO.tapOnWhyDoYouFeelLikeThatOption();
        whatsHappenedPO.tapOnItsMyTreatmentTextView();
        whatsHappenedPO.tapOnseeMoreOptionButton();
        whatsHappenedPO.checkIHaveHadSomeGoodNewsCheckBox();

        emotionPO = whatsHappenedPO.tapOnSaveButton();

        WhatMightHelpPO whatMightHelpPO = emotionPO.tapOnWhatMightHelpPlusIcon();
        whatMightHelpPO.tapOnSeeAllOptionsButton();
        whatMightHelpPO.tapOnMyTreatmentOption();
        whatMightHelpPO.scrollDownToScreen();
        whatMightHelpPO.tapOnParticularFilteredOptions(2);

        emotionPO = whatMightHelpPO.tapOnSaveButton();

        OverviewPO overviewPO = emotionPO.tapOnIAmFinishedButton();
        overviewPO.tapOnHamburgerIcon();

        MyProfilePO myProfilePO = overviewPO.tapOnMyProfileOption();
        myProfilePO.tapOnMyAccountOption();

        mainScreenPO = myProfilePO.doLogOut();

        Assert.assertTrue(mainScreenPO.isMainScreenDisplayed(), "Main Screen did not display, LogOut might have failed!");
    }

    @BeforeTest
    @Override
    public void setUpPage() throws MalformedURLException {
        androidDriver = new AndroidDriver(new URL(APPIUM_SERVER_URL), getDesiredCapabilitiesForAndroid());
    }
}
