1) Please import as the gradle project on IntelliJ Idea/Eclipse IDE (IntelliJ prefered) 

2) Run the build.gradle to make sure all dependencies executes

3) Move to: https://bitbucket.org/prat3ik/emotionspace-android-appium/src/master/src/test/resources/configuration.properties and change the 1. android.platform.version and 2. android.device.name according to your connected device. (It will work on both physical device and android emulator)

4) Run the test case written in: https://bitbucket.org/prat3ik/emotionspace-android-appium/src/master/src/test/java/testcases/AndroidTest.java (Make sure TestNG is installed)
